package com.narcisse.sqlite;

/**
 * Created by pkafka on 1/4/14.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.narcisse.habbits.Habit;
import com.narcisse.habbits.HabitGroup;

import java.util.ArrayList;
import java.util.Date;

public class HabitsDataSource {

    // Database fields
    private MySQLiteHelper dbHelper;
    private String[] allColumnsHabit = {
                MySQLiteHelper.COLUMN_ID,
                MySQLiteHelper.COLUMN_ACTIVE,
                MySQLiteHelper.COLUMN_DESCRIPTION,
                MySQLiteHelper.COLUMN_GROUP_ID,
                MySQLiteHelper.COLUMN_EXPIRES };

    private String[] allColumnsGroups = {
            MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_DESCRIPTION };

    public HabitsDataSource(Context context){
        dbHelper = new MySQLiteHelper(context);
    }

    public void dumpAllHabbitsAndGroups(){
        dbHelper.onUpgrade(dbHelper.getWritableDatabase(),
                MySQLiteHelper.DATABASE_VERSION,
                MySQLiteHelper.DATABASE_VERSION);
    }

    public void close() {
        dbHelper.close();
    }

    public Habit insertHabit(Habit habit) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_DESCRIPTION, habit.description);
        values.put(MySQLiteHelper.COLUMN_GROUP_ID, habit.groupId);
        values.put(MySQLiteHelper.COLUMN_ACTIVE, habit.active ? 1 : 0);
        values.put(MySQLiteHelper.COLUMN_EXPIRES, habit.expiresOn.getTime());
        long id = dbHelper.getWritableDatabase().insert(MySQLiteHelper.TABLE_HABITS, null, values);
        habit.id = (int)id;
        return habit;
    }

    public HabitGroup insertGroup(HabitGroup group) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_DESCRIPTION, group.description);
        long id = dbHelper.getWritableDatabase().insert(MySQLiteHelper.TABLE_GROUPS, null, values);
        group.id = (int)id;
        return group;
    }

    public double getAverageRemaining(){
        Cursor cursor = getAllHabitsCursor();
        if (cursor.getCount() < 1) return -1;

        cursor.moveToFirst();
        Habit h = cursorToHabit(cursor);
        int total = h.getExpireLevel();
        while(cursor.moveToNext()){
            total += cursorToHabit(cursor).getExpireLevel();
        }

        return total / cursor.getCount();
    }

    public void removeExpiredHabits(){
        Cursor cursor = getAllHabitsCursor();
        if (cursor.getCount() < 1) return;

        ArrayList<Habit> toDelete = new ArrayList<Habit>();

        cursor.moveToFirst();
        Habit h = cursorToHabit(cursor);
        if (h.active && h.expiresOn.getTime() < new Date().getTime()) toDelete.add(h);
        while(cursor.moveToNext()){
            if (h.active && h.expiresOn.getTime() < new Date().getTime()) toDelete.add(h);
        }

        for (Habit hToDelete : toDelete) deleteHabit(hToDelete.id);
    }

    public Habit getHabit(int id){
        Cursor cursor = dbHelper.getReadableDatabase().query(MySQLiteHelper.TABLE_HABITS,
                allColumnsHabit, MySQLiteHelper.COLUMN_ID + " = '" + id + "'", null,
                null, null, null);
        cursor.moveToFirst();
        Habit newHabit = cursorToHabit(cursor);
        cursor.close();
        return newHabit;
    }

    public Cursor getAllHabitsCursor(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        return db.query(MySQLiteHelper.TABLE_HABITS,
                allColumnsHabit, null, null, null, null, MySQLiteHelper.COLUMN_EXPIRES);
    }

    public Cursor getHabitsByGroupCursor(int groupId){
        Cursor cursor = dbHelper.getReadableDatabase().query(MySQLiteHelper.TABLE_HABITS,
                allColumnsHabit, MySQLiteHelper.COLUMN_GROUP_ID + " = '" + groupId + "'", null,
                null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public Cursor getGroupsCursor(){
        Cursor cursor = dbHelper.getReadableDatabase().query(MySQLiteHelper.TABLE_GROUPS,
                allColumnsGroups, null, null,
                null, null, null);
        cursor.moveToFirst();
        return cursor;
    }

    public void updateHabit(Habit habit){
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_ID, habit.id);
        values.put(MySQLiteHelper.COLUMN_GROUP_ID, habit.groupId);
        values.put(MySQLiteHelper.COLUMN_ACTIVE, habit.active ? 1 : 0);
        values.put(MySQLiteHelper.COLUMN_DESCRIPTION, habit.description);
        values.put(MySQLiteHelper.COLUMN_EXPIRES, habit.expiresOn.getTime());

        dbHelper.getWritableDatabase().replace(MySQLiteHelper.TABLE_HABITS
                ,null
                ,values);
    }

    public void deleteHabit(int id){
        dbHelper.getWritableDatabase().delete(
                MySQLiteHelper.TABLE_HABITS,
                MySQLiteHelper.COLUMN_ID + " = '" + id + "'", null);
    }

    public void deleteGroup(int groupId){
        dbHelper.getWritableDatabase().delete(
                MySQLiteHelper.TABLE_HABITS,
                MySQLiteHelper.COLUMN_GROUP_ID + " = '" + groupId + "'", null);

        dbHelper.getWritableDatabase().delete(
                MySQLiteHelper.TABLE_GROUPS,
                MySQLiteHelper.COLUMN_ID + " = '" + groupId + "'", null);
    }

    public static Habit cursorToHabit(Cursor cursor) {
        Habit habit = new Habit();
        habit.id = cursor.getInt(cursor.getColumnIndex(MySQLiteHelper.COLUMN_ID));
        habit.description = cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_DESCRIPTION));
        if (cursor.getInt(cursor.getColumnIndex(MySQLiteHelper.COLUMN_ACTIVE)) == 1) habit.active = true;
        habit.groupId = cursor.getInt(cursor.getColumnIndex(MySQLiteHelper.COLUMN_GROUP_ID));
        habit.expiresOn = new Date(cursor.getLong(cursor.getColumnIndex(MySQLiteHelper.COLUMN_EXPIRES)));
        return habit;
    }

    public static HabitGroup cursorToGroup(Cursor cursor) {
        HabitGroup group = new HabitGroup();
        group.id = cursor.getInt(cursor.getColumnIndex(MySQLiteHelper.COLUMN_ID));
        group.description = cursor.getString(cursor.getColumnIndex(MySQLiteHelper.COLUMN_DESCRIPTION));
        return group;
    }
}
