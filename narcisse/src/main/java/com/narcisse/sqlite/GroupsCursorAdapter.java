package com.narcisse.sqlite;


import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.narcisse.habbits.HabitGroup;
import com.narcisse.habbits.MainActivity;
import com.narcisse.habbits.R;

/**
 * Created by pkafka on 1/4/14.
 */
public class GroupsCursorAdapter extends CursorAdapter {

    LayoutInflater mInflater;
    Context mContext;
    MainActivity mParent;

    public GroupsCursorAdapter(Context context, Cursor c, MainActivity parent) {
        super(context, c, true);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mParent = parent;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return null;
    }
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

    }

    @Override
    public HabitGroup getItem(int position) {
        mCursor.moveToPosition(position);
        return HabitsDataSource.cursorToGroup(mCursor);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null)
            convertView = createView(parent);

        holder = (ViewHolder)convertView.getTag();

        mCursor.moveToPosition(position);
        holder.group = HabitsDataSource.cursorToGroup(mCursor);
        holder.tvDescription.setText(holder.group.description);

        return convertView;
    }

    public View createView(ViewGroup parent){
        View v = mInflater.inflate(R.layout.group_row, parent, false);

        ViewHolder holder = new ViewHolder();
        holder.tvDescription = (TextView)v.findViewById(R.id.tvDescription);

        v.setTag(holder);
        return v;
    }

    public class ViewHolder{
        public HabitGroup group;
        TextView tvDescription;
    }
}
