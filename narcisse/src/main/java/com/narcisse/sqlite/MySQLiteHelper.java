package com.narcisse.sqlite;

/**
 * Created by pkafka on 1/4/14.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "HABBITS";


    static final String TABLE_HABITS = "Habbits";

    static final String COLUMN_ID = "_id";
    static final String COLUMN_GROUP_ID = "groupId";
    static final String COLUMN_DESCRIPTION = "description";
    static final String COLUMN_ACTIVE = "active";
    static final String COLUMN_EXPIRES = "expiresOn";

    static final String COLUMN_ALARM_ENABLED = "alarmEnabled";
    static final String COLUMN_ALARM_TIME = "alarmTime";

    private static final String TABLE_CREATE_HABITS =
            "CREATE TABLE " + TABLE_HABITS + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_GROUP_ID + " INTEGER, " +
                    COLUMN_DESCRIPTION + " TEXT, " +
                    COLUMN_ACTIVE + " INTEGER, " +
                    COLUMN_EXPIRES + " REAL);";

    static final String TABLE_GROUPS = "Groups";
    private static final String TABLE_CREATE_GROUPS =
            "CREATE TABLE " + TABLE_GROUPS + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_ALARM_ENABLED + " INTEGER, " +
                    COLUMN_ALARM_TIME + " REAL, " +
                    COLUMN_DESCRIPTION + " TEXT);";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE_GROUPS);
        db.execSQL(TABLE_CREATE_HABITS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HABITS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUPS);
        onCreate(db);
    }

}