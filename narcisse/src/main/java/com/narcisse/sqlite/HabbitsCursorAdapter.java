package com.narcisse.sqlite;


import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.narcisse.habbits.Habit;
import com.narcisse.habbits.HabitListFrag;
import com.narcisse.habbits.R;

/**
 * Created by pkafka on 1/4/14.
 */
public class HabbitsCursorAdapter extends CursorAdapter {

    LayoutInflater mInflater;
    Context mContext;
    HabitListFrag mParent;

    public HabbitsCursorAdapter(Context context, Cursor c, HabitListFrag parent) {
        super(context, c, true);
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mParent = parent;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return null;
    }
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

    }

    @Override
    public Habit getItem(int position) {
        mCursor.moveToPosition(position);
        return HabitsDataSource.cursorToHabit(mCursor);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null)
            convertView = createView(parent);

        holder = (ViewHolder)convertView.getTag();

        mCursor.moveToPosition(position);
        holder.habbit = HabitsDataSource.cursorToHabit(mCursor);
        holder.tvDescription.setText(holder.habbit.description);

        int expLevel = holder.habbit.getExpireLevel();
        holder.imgSmiley.setImageDrawable(
                Habit.getSmileyDrawable(mContext.getResources(), expLevel));

        if (!holder.habbit.active){
            holder.cbx.setChecked(false);
            holder.cbx.setEnabled(true);
            holder.imgSmiley.setImageDrawable(null);
        }
        else if (expLevel == 0 && holder.habbit.active) {
            holder.cbx.setChecked(true);
            holder.cbx.setEnabled(false);
        }
        else {
            holder.cbx.setChecked(false);
            holder.cbx.setEnabled(true);
        }

        holder.cbx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mParent.completeHabbit(holder.habbit);
            }
        });

        holder.spinner.setTag(holder.habbit);
        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch(i){
                    case 1:
                        mParent.deleteHabit(holder.habbit);
                        break;
                    case 2:
                        mParent.editHabit(holder.habbit);
                        break;
                    case 3:
                        mParent.resetHabit(holder.habbit);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return convertView;
    }

    public View createView(ViewGroup parent){
        View v = mInflater.inflate(R.layout.habbit_row, parent, false);

        ViewHolder holder = new ViewHolder();
        holder.tvDescription = (TextView)v.findViewById(R.id.tvDescription);
        holder.imgSmiley = (ImageView)v.findViewById(R.id.imgSmiley);
        holder.cbx = (CheckBox)v.findViewById(R.id.cbx);
        holder.spinner = (Spinner)v.findViewById(R.id.spinner);

        v.setTag(holder);
        return v;
    }

    public class ViewHolder{
        public Habit habbit;
        CheckBox cbx;
        TextView tvDescription;
        ImageView imgSmiley;
        Spinner spinner;
    }
}
