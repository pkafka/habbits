package com.narcisse.habbits;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.narcisse.sqlite.HabbitsCursorAdapter;
import com.narcisse.sqlite.HabitsDataSource;


public class HabitListFrag extends Fragment implements UndoBarController.UndoListener
        , LoaderManager.LoaderCallbacks<Cursor> {

    ListView mListHabbits;
    HabbitsCursorAdapter mAdapter;
    UndoBarController mUndoBarController;
    HabitGroup mGroup;

    public static HabitListFrag newInstance() {
        HabitListFrag fragment = new HabitListFrag();
        return fragment;
    }

    public HabitListFrag() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_habit_list, container, false);
        mListHabbits = (ListView)rootView.findViewById(R.id.lvHabbits);
        loadHabits();

        SwipeDismissListViewTouchListener touchListener =
                new SwipeDismissListViewTouchListener(
                        mListHabbits,
                        new SwipeDismissListViewTouchListener.DismissCallbacks() {
                            @Override
                            public boolean canDismiss(int position) {
                                return true;
                            }

                            @Override
                            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    Habit toDelete = mAdapter.getItem(position);
                                    new HabitsDataSource(getActivity()).deleteHabit(toDelete.id);
                                    refresh();
                                    mUndoBarController.showUndoBar(true, "Deleted", toDelete);
                                }
                                mAdapter.notifyDataSetChanged();
                            }
                        });
        mListHabbits.setOnTouchListener(touchListener);
        mListHabbits.setOnScrollListener(touchListener.makeScrollListener());
        mUndoBarController = new UndoBarController(rootView.findViewById(R.id.undobar), this);

        return rootView;
    }

    public void loadHabits(){
        mGroup = ((MainActivity)getActivity()).mHabitGroupSelected;
        if (mGroup == null) return;

        mAdapter = new HabbitsCursorAdapter(getActivity(), null, this);
        mListHabbits.setAdapter(mAdapter);
        getLoaderManager().initLoader(20, null, this);
        refresh();
    }

    public void deleteHabit(Habit h){
        new HabitsDataSource(getActivity()).deleteHabit(h.id);
        refresh();
    }

    public void completeHabbit(Habit h){
        h.expiresOn = Habit.getNewExperation();
        h.active = true;
        new HabitsDataSource(getActivity()).updateHabit(h);
        refresh();
    }

    public void resetHabit(Habit h){
        h.expiresOn = Habit.getNewExperation();
        h.active = false;
        new HabitsDataSource(getActivity()).updateHabit(h);
        refresh();
    }

    public void editHabit(Habit h){
        ((MainActivity)getActivity()).editHabit(h);
    }

    public void refresh(){
        mAdapter.notifyDataSetChanged();
        getLoaderManager().restartLoader(20, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
        if (mGroup == null) return null;
        return new HabbitsCursorLoader(getActivity(), mGroup.id);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        mAdapter.swapCursor(null);
    }

    public static final class HabbitsCursorLoader extends CursorLoader {

        Context mContext;
        int mGroupId;

        public HabbitsCursorLoader(Context context, int groupId) {
            super(context);
            mGroupId = groupId;
            mContext = context;
        }

        @Override
        public Cursor loadInBackground() {
            HabitsDataSource ds = new HabitsDataSource(mContext);
            return ds.getHabitsByGroupCursor(mGroupId);
        }

    }

    @Override
    public void onUndo(Parcelable token) {
        new HabitsDataSource(getActivity()).insertHabit((Habit) token);
        refresh();
    }

}
