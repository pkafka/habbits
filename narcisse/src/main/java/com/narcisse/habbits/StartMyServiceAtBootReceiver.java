package com.narcisse.habbits;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by pkafka on 1/5/14.
 */
public class StartMyServiceAtBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            MainActivity.removeExpiredHabbits(context);
            MainActivity.setNotification(context);
            MainActivity.setAlarms(context);
        //}
    }
}
