package com.narcisse.habbits;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.narcisse.sqlite.GroupsCursorAdapter;
import com.narcisse.sqlite.HabitsDataSource;

import java.util.Calendar;

public class MainActivity extends FragmentActivity {

    HabitListFrag mHabitListFrag;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private GroupsCursorAdapter mAdapter;

    HabitGroup mHabitGroupSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_navigation_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            public void onDrawerClosed(View view) {
                if (mHabitGroupSelected == null) getActionBar().setTitle("Habits");
                else getActionBar().setTitle(mHabitGroupSelected.description);
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Lists");
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mAdapter = new GroupsCursorAdapter(this, new HabitsDataSource(this).getGroupsCursor(), this);
        mDrawerList.setAdapter(mAdapter);

        mHabitListFrag = HabitListFrag.newInstance();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.content_frame, mHabitListFrag)
                .commit();

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mHabitGroupSelected = ((GroupsCursorAdapter.ViewHolder)view.getTag()).group;
                mHabitListFrag.loadHabits();
                mDrawerList.setItemChecked(i, true);
                setTitle(mHabitGroupSelected.description);
                mDrawerLayout.closeDrawers();
            }
        });
        mDrawerList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                HabitGroup g = ((GroupsCursorAdapter.ViewHolder)view.getTag()).group;
                new HabitsDataSource(MainActivity.this).deleteGroup(g.id);
                mAdapter = new GroupsCursorAdapter(MainActivity.this,
                        new HabitsDataSource(MainActivity.this).getGroupsCursor(),
                        MainActivity.this);
                mDrawerList.setAdapter(mAdapter);
                Toast.makeText(MainActivity.this, "Deleted Group", Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        mDrawerLayout.openDrawer(findViewById(R.id.drawer));

        Button btnAddGroup = (Button)findViewById(R.id.btnAddGroup);
        btnAddGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewGroupFrag.newInstance().show(
                        getSupportFragmentManager().beginTransaction(), "dialog");
            }
        });

        removeExpiredHabbits(this);
        setAlarms(this);
        setNotification(this);
    }

    static void removeExpiredHabbits(Context c){
        new HabitsDataSource(c).removeExpiredHabits();
    }

    static void setAlarms(Context c){

        AlarmManager alarmMgr = (AlarmManager)c.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(c, StartMyServiceAtBootReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(c, 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        HabitsDataSource ds = new HabitsDataSource(c);
        Cursor cursor = ds.getAllHabitsCursor();
        if (cursor.moveToFirst()){
            Habit oldestHabbit = HabitsDataSource.cursorToHabit(cursor);
            calendar.setTime(oldestHabbit.expiresOn);
            alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, alarmIntent);
        }
    }

    static void setNotification(Context c){

        double avg = new HabitsDataSource(c).getAverageRemaining();
        //drop the decimal or round?

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(c)
                        .setSmallIcon(Habit.getSmileyDrawableId((int) avg))
                        .setContentTitle("Narcisse Habbits")
                        .setContentText("You have active habbits to complete...");

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(c, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(c);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(0, mBuilder.build());
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) return true;

        int id = item.getItemId();
        if (id == R.id.action_add) {
            if (mDrawerList.getAdapter().isEmpty() || mHabitGroupSelected == null){
                Toast.makeText(MainActivity.this, "Select a group first", Toast.LENGTH_SHORT).show();
                return true;
            }
            NewHabbitFrag.newInstance().show(getSupportFragmentManager().beginTransaction(), "dialog");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addHabbit(Habit habbit){
        habbit.groupId = mHabitGroupSelected.id;
        new HabitsDataSource(this).insertHabit(habbit);
        mHabitListFrag.refresh();
        Toast.makeText(MainActivity.this, "added: " + habbit.description, Toast.LENGTH_SHORT).show();
    }

    public void addGroup(HabitGroup group){
        new HabitsDataSource(this).insertGroup(group);
        mDrawerList.setAdapter(new GroupsCursorAdapter(this,
                new HabitsDataSource(this).getGroupsCursor(), this));
        Toast.makeText(MainActivity.this, "new group: " + group.description, Toast.LENGTH_SHORT).show();
    }

    public void editHabit(Habit h){
        NewHabbitFrag frag = NewHabbitFrag.newInstance();
        frag.mEditing = h;
        frag.show(getSupportFragmentManager().beginTransaction(), "dialog");
    }



}
