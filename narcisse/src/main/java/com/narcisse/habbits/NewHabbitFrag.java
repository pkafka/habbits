package com.narcisse.habbits;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.narcisse.sqlite.HabitsDataSource;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Use the {@link NewHabbitFrag#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class NewHabbitFrag extends DialogFragment {

    public Habit mEditing;

    public static NewHabbitFrag newInstance() {
        NewHabbitFrag fragment = new NewHabbitFrag();
        return fragment;
    }
    public NewHabbitFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_habbit, container, false);
        getDialog().setTitle("New Habit");
        final EditText editTextDesc = (EditText)v.findViewById(R.id.editTextDescription);
        if (mEditing != null) editTextDesc.setText(mEditing.description);
        Button btnAdd = (Button)v.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mEditing != null){
                    mEditing.description = editTextDesc.getText().toString();
                    new HabitsDataSource(getActivity()).updateHabit(mEditing);
                    ((MainActivity)getActivity()).mHabitListFrag.refresh();
                }
                else {
                    Habit h = new Habit();
                    h.description = editTextDesc.getText().toString();
                    h.active = false;
                    h.expiresOn = Habit.getNewExperation();
                    ((MainActivity)getActivity()).addHabbit(h);
                }

                dismiss();
            }
        });

        return v;
    }


}
