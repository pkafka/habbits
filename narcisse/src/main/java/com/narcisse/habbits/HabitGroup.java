package com.narcisse.habbits;

import com.google.gson.Gson;

/**
 * Created by pkafka on 1/4/14.
 */
public class HabitGroup {

    public int id;
    public String description;

    public static HabitGroup fromJson(String json){
        return new Gson().fromJson(json, HabitGroup.class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this, this.getClass());
    }


}
