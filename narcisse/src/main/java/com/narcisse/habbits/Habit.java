package com.narcisse.habbits;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by pkafka on 1/4/14.
 */
public class Habit implements Parcelable {

    public int id;
    public int groupId;
    public String description;
    public Date expiresOn;
    public boolean active;

    public static Habit fromJson(String json){
        return new Gson().fromJson(json, Habit.class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this, this.getClass());
    }
//test
    public int getExpireLevel(){
        Long now = new Date().getTime();
        Long expires = expiresOn.getTime();
        Long remaining = expires - now;

        if (remaining > 60 * 1000 * 60 * 96) return 0;
        else if (remaining > 60 * 1000 * 60 * 72) return 1;
        else if (remaining > 60 * 1000 * 60 * 48) return 2;
        else if (remaining > 60 * 1000 * 60 * 24) return 3;
        else return 4;
    }

    public static Drawable getSmileyDrawable(Resources r, int expLevel){
        return r.getDrawable(getSmileyDrawableId(expLevel));
    }

    public static int getSmileyDrawableId(int expLevel){
        if (expLevel == 0) return R.drawable.smiley_green_teeth_2;
        else if (expLevel == 1) return R.drawable.smiley_green_2;
        else if (expLevel == 2) return R.drawable.smiley_yellow_2;
        else if (expLevel == 3) return R.drawable.smiley_red_2;
        else return R.drawable.smiley_gray_2;
    }

    public static Date getNewExperation(){
        //set to midnight and add 96 hours
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 1);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        cal.add(Calendar.HOUR, 96);

        return cal.getTime();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(toString());
    }

    public static final Parcelable.Creator<Habit> CREATOR
            = new Parcelable.Creator<Habit>() {
        public Habit createFromParcel(Parcel in) {
            return fromJson(in.readString());
        }

        public Habit[] newArray(int size) {
            return new Habit[size];
        }
    };

}
