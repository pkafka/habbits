package com.narcisse.habbits;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Use the {@link com.narcisse.habbits.NewGroupFrag#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class NewGroupFrag extends DialogFragment {

    public static NewGroupFrag newInstance() {
        NewGroupFrag fragment = new NewGroupFrag();
        return fragment;
    }
    public NewGroupFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_habbit, container, false);
        getDialog().setTitle("New Habit Group");
        final EditText editTextDesc = (EditText)v.findViewById(R.id.editTextDescription);

        Button btnAdd = (Button)v.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HabitGroup g = new HabitGroup();
                g.description = editTextDesc.getText().toString();
                ((MainActivity)getActivity()).addGroup(g);

                dismiss();
            }
        });

        return v;
    }


}
